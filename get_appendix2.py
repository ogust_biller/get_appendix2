import os
import sys
import click
import pymssql
import queue
import threading
import time
import openpyxl
from operator import itemgetter
from pprint import pprint
from decimal import Decimal
import locale
import datetime

invoiced_sums = []


def nz(value):
    """
    function return 0 if value is None, else return value
    """
    if value is None:
        return {'P':0,'P_URGENCY':0,'P_ILLNESS':0, 'P_PREVENTION':0, 'S':0, 'S_HMP':0, 'Z':0, 'A':0}
    else:
        return value


def zip_dicts(dict1,dict2):
    if len(dict1) > 0 and len(dict2) == 0: return dict1
    if len(dict2) > 0 and len(dict1) == 0: return dict2
    all_lpu=set((*list(dict1.keys()),*list(dict2.keys())))
    return {x: {'P': nz(dict1.get(x))['P'] + nz(dict2.get(x))['P'],
                'P_PREVENTION': nz(dict1.get(x))['P_PREVENTION'] + nz(dict2.get(x))['P_PREVENTION'],
                'P_URGENCY': nz(dict1.get(x))['P_URGENCY'] + nz(dict2.get(x))['P_URGENCY'],
                'P_ILLNESS': nz(dict1.get(x))['P_ILLNESS'] + nz(dict2.get(x))['P_ILLNESS'],
                'S': nz(dict1.get(x))['S'] + nz(dict2.get(x))['S'],
                'S_HMP': nz(dict1.get(x))['S_HMP'] + nz(dict2.get(x))['S_HMP'],
                'Z': nz(dict1.get(x))['Z'] + nz(dict2.get(x))['Z'],
                'A': nz(dict1.get(x))['A'] + nz(dict2.get(x))['A']
               } for x in all_lpu
            }



def template_resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def exectypicalqueries(cursr, querystring, q):
    cursr.execute(querystring)
    q.put_nowait(list(cursr))


def printmsg(msg, cursr, querystring):
    qe = queue.Queue()
    thread = threading.Thread(target=exectypicalqueries, args=(cursr, querystring, qe))
    thread.start()

    eli_count = 0
    while thread.is_alive():
        print(msg, '.'*(eli_count+1), ' '*(2-eli_count), end='\r')
        eli_count = (eli_count + 1) % 3
        time.sleep(0.2)
    thread.join()
    print(msg,'...Ok\n')
    return qe.get()

@click.command()
@click.argument('smo')
@click.argument('report_filename')
@click.option('--date1','-d1', default ="month's start", help = 'Дата начала загрузки счетов в формате "yyyy-mm-dd" (по умолчанию - первое число текущего месяца')
@click.option('--date2','-d2', default ='now', help = 'Дата начала загрузки счетов в формате "yyyy-mm-dd" (по умолчанию - первое число текущего месяца')
@click.option('--period-for-remek', '-p', default = 'this month', help = 'Период отчета для выборки по повторному МЭКу в формате "yyyymm"(по умолчанию - текущий месяц')
@click.option('--rep-template', '-t', default = "Приложение 2 к приказу.xlsx::Сведения (31.03.2021)[E12:AD159]", help='Имя файла с шаблоном отчета синформацией о рабочем диапазоне в формате "filename::sheetname[A1:Z28]"')
@click.option('--database-server', '-s', default = 'OBL-SRZ-2', help='Имя хоста с БД')
@click.option('--database-name', '-n', default = 'IESDB', help='Имя БД')
@click.option('--database-user', '-u', default = 'sa', help='Имя пользователя БД ')
@click.option('--database-user-password', '-pass', default = 'sa', help='Пароль пользователя БД')
def main(smo, report_filename, date1, date2, period_for_remek , rep_template, database_server, database_name, database_user,database_user_password ):
    """
    Формирование отчета по Приложению №2 к Приказу №06\n
    SMO - код СМО или 'F' - для иногородних\n
    REPORT_FILENAME - имя файла (будет создан) в который будет сохранен отчет\n
    *ВНИМАНИЕ: соответствие кодов ЛПУ номерам строк получаем с листа\n
               с именем "Список ЛПУ", в котором в первом столбце прописаны\n
               коды ЛПУ на тех же строках, на которых в рабочем листе должна\n
               выводиться информация по соответствующим ЛПУ\n
    """
    #всякая там инициализация
    if date1 == "month's start":
        date1 = datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0).strftime('%Y%m%d')
    else:
        date1 = datetime.datetime.strptime(date1, '%Y-%m-%d').strftime('%Y%m%d')

    locale.setlocale(locale.LC_ALL, 'ru')

    if date2 == "now":
        month = (datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0)-datetime.timedelta(days=1)).strftime('%B')
    else:
        month = (datetime.datetime.strptime(date2, '%Y-%m-%d').replace(day=1, hour=0, minute=0, second=0) - datetime.timedelta(
            days=1)).strftime('%B')

    if date2 == "now":
        year = (datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0)-datetime.timedelta(days=1)).strftime('%Y')
    else:
        year = (datetime.datetime.strptime(date2, '%Y-%m-%d').replace(day=1, hour=0, minute=0, second=0) - datetime.timedelta(
            days=1)).strftime('%Y')


    if date2 == "now":
        date2 = datetime.datetime.utcnow().strftime('%Y%m%d')
    else:
        date2 = datetime.datetime.strptime(date2, '%Y-%m-%d').strftime('%Y%m%d')

    if period_for_remek == 'this month':
        period_for_remek= datetime.datetime.now().strftime('%Y%m')

    if rep_template=='Приложение 2 к приказу.xlsx::Сведения (31.03.2021)[E12:AD159]':
        rep_template=template_resource_path(r'templates\Приложение 2 к приказу.xlsx')
        sheetname = 'Сведения (31.03.2021)'
        rangestr = 'E12:AD159'
    else:
        rangestr = rep_template[rep_template.index('[')+1:rep_template.index(']')]
        sheetname = rep_template[rep_template.index('::') + 2:rep_template.index('[')]
        rep_template = rep_template[:rep_template.index('::') ]


    locale.setlocale(locale.LC_ALL, '')
    locale._override_localeconv = {'mon_thousands_sep': ' '}

    conn = pymssql.connect(host=database_server, user=database_user, password=database_user_password,database=database_name, charset='WINDOWS-1251')
    cursor = conn.cursor(as_dict=True)

    #выбираем выставленные суммы ...
    sqlstring = f'''
    SELECT a.LPU, a.P, a.P_PREVENTION, a.P_URGENCY, a.P_ILLNESS, a.S, a.S_HMP, a.Z, a.A
    from 
    (SELECT sa.LPU, 
        COALESCE(sum(CASE WHEN sa.USL_OK = 3 OR  sa.USL_OK  IS NULL THEN sa.SUMV END), 0) P,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '1.0' OR sa.P_CEL_ = '1.2' OR sa.P_CEL_ = '1.3' OR sa.P_CEL_ like '2.%') THEN sa.SUMV END), 0) P_PREVENTION,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '1.1' ) THEN sa.SUMV END), 0) P_URGENCY,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '3.0' ) THEN sa.SUMV END), 0) P_ILLNESS,
        COALESCE(sum(CASE WHEN sa.USL_OK = 1 THEN sa.SUMV END), 0) S,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 1) and (sa.VID_HMP='HMP')  THEN sa.SUMV END), 0) S_HMP,
        COALESCE(sum(CASE WHEN sa.USL_OK = 2 THEN sa.SUMV END), 0) Z,
        COALESCE(sum(CASE WHEN sa.USL_OK = 4 THEN sa.SUMV END), 0) A
    FROM (
        SELECT sa_.*, (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.VID_HMP IS NOT NULL AND ss_.IsDelete=0 ) THEN 'HMP' ELSE NULL END) VID_HMP,
                      (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND v025_.IDPC = '1.1' ) THEN '1.1'
                            WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND (v025_.IDPC = '1.0' OR v025_.IDPC = '1.2' OR v025_.IDPC = '1.3' OR v025_.IDPC like '2.%') ) THEN '1.0'
                       ELSE '3.0' END) P_CEL_
        FROM IES.T_SCHET_SLUCH_ACCOMPLISHED sa_
    ) sa
    LEFT JOIN IES.T_SCHET_ZAP sz ON sz.SchetZapID = sa.SchetZap
    LEFT JOIN IES.T_SCHET s ON sz.Schet=s.SchetID            
    WHERE s.ReceivedDate>='{date1}' AND s.ReceivedDate<='{date2}' AND
        sz.SMO='{smo}' AND
        s.type_=693 AND 
        sa.IsDelete=0  
    GROUP BY sa.LPU) a
    '''
    global invoiced_sums
    invoiced_sums = printmsg('[1/3]выбираем выставленные суммы',cursor,sqlstring)
    invoiced_sums={x['LPU']:x for x in invoiced_sums}

    ##whatsfack!
    #выбираем первичный МЭК
    sqlstring = f'''
    SELECT a.LPU, a.P, a.P_PREVENTION, a.P_URGENCY, a.P_ILLNESS, a.S, a.S_HMP, a.Z, a.A
    from 
    (SELECT sa.LPU, 
        COALESCE(sum(CASE WHEN sa.USL_OK = 3 OR  sa.USL_OK  IS NULL THEN sa.SUMV END), 0) P,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '1.0' OR sa.P_CEL_ = '1.2' OR sa.P_CEL_ = '1.3' OR sa.P_CEL_ like '2.%') THEN sa.SUMV END), 0) P_PREVENTION,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '1.1' ) THEN sa.SUMV END), 0) P_URGENCY,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 3 OR  sa.USL_OK  IS NULL) AND  (sa.P_CEL_ = '3.0' ) THEN sa.SUMV END), 0) P_ILLNESS,
        COALESCE(sum(CASE WHEN sa.USL_OK = 1 THEN sa.SUMV END), 0) S,
        COALESCE(sum(CASE WHEN (sa.USL_OK = 1) and (sa.VID_HMP='HMP')  THEN sa.SUMV END), 0) S_HMP,
        COALESCE(sum(CASE WHEN sa.USL_OK = 2 THEN sa.SUMV END), 0) Z,
        COALESCE(sum(CASE WHEN sa.USL_OK = 4 THEN sa.SUMV END), 0) A
    FROM (
            SELECT sa_.*, (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.VID_HMP IS NOT NULL AND ss_.IsDelete=0 ) THEN 'HMP' ELSE NULL END) VID_HMP,
                          (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND v025_.IDPC = '1.1' ) THEN '1.1'
                                WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa_.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND (v025_.IDPC = '1.0' OR v025_.IDPC = '1.2' OR v025_.IDPC = '1.3' OR v025_.IDPC like '2.%') ) THEN '1.0'
                           ELSE '3.0' END) P_CEL_
            FROM IES.T_SCHET_SLUCH_ACCOMPLISHED sa_
        ) sa
    LEFT JOIN IES.T_SCHET_ZAP sz ON sz.SchetZapID = sa.SchetZap
    LEFT JOIN IES.T_SCHET s ON sz.Schet=s.SchetID			
    WHERE sa.SUMP=0 AND 
    	s.ReceivedDate>='{date1}' AND s.ReceivedDate<='{date2}' AND
    	sz.SMO='{smo}' AND
    	s.type_=693 AND     	
    	sa.IsDelete=0  
    GROUP BY sa.LPU) a
    '''
    global primary_mek_sums
    primary_mek_sums = printmsg('[2/3]выбираем первичный МЭК', cursor, sqlstring)
    primary_mek_sums = {x['LPU']: x for x in primary_mek_sums}

    #Выбираем повторный МЭК
    sqlstring = f'''
    SELECT a.LPU, a.P, a.P_PREVENTION, a.P_URGENCY, a.P_ILLNESS, a.S,a.S_HMP, a.Z, a.A
    FROM 
        (SELECT sa_.LPU, 
            COALESCE(sum(CASE WHEN sa_.USL_OK = 3 OR  sa_.USL_OK  IS NULL THEN sa_.SUMV END), 0) P,
            COALESCE(sum(CASE WHEN (sa_.USL_OK = 3 OR  sa_.USL_OK  IS NULL) AND  (sa_.P_CEL_ = '1.0' OR sa_.P_CEL_ = '1.2' OR sa_.P_CEL_ = '1.3' OR sa_.P_CEL_ like '2.%') THEN sa_.SUMV END), 0) P_PREVENTION,
            COALESCE(sum(CASE WHEN (sa_.USL_OK = 3 OR  sa_.USL_OK  IS NULL) AND  (sa_.P_CEL_ = '1.1' ) THEN sa_.SUMV END), 0) P_URGENCY,
            COALESCE(sum(CASE WHEN (sa_.USL_OK = 3 OR  sa_.USL_OK  IS NULL) AND  (sa_.P_CEL_ = '3.0' ) THEN sa_.SUMV END), 0) P_ILLNESS,
            COALESCE(sum(CASE WHEN sa_.USL_OK = 1 THEN sa_.SUMV END), 0) S,
            COALESCE(sum(CASE WHEN (sa_.USL_OK = 1) and (sa_.VID_HMP='HMP')  THEN sa_.SUMV END), 0) S_HMP,
            COALESCE(sum(CASE WHEN sa_.USL_OK = 2 THEN sa_.SUMV END), 0) Z,
            COALESCE(sum(CASE WHEN sa_.USL_OK = 4 THEN sa_.SUMV END), 0) A
        FROM (    
        SELECT sa.LPU,sa.USL_OK,sa.SUMV,
               (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ WHERE ss_.SchetSluchAccomplished=sa.SchetSluchAccomplishedID AND ss_.VID_HMP IS NOT NULL AND ss_.IsDelete=0 ) THEN 'HMP' ELSE NULL END) VID_HMP,
                                  (CASE WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND v025_.IDPC = '1.1' ) THEN '1.1'
                                        WHEN  EXISTS (SELECT ss_.VID_HMP VID_HMP from IES.T_SCHET_SLUCH ss_ LEFT JOIN IES.T_V025_KPC v025_ ON v025_.V025KpcID = ss_.P_CEL WHERE ss_.SchetSluchAccomplished=sa.SchetSluchAccomplishedID AND ss_.IsDelete=0 AND (v025_.IDPC = '1.0' OR v025_.IDPC = '1.2' OR v025_.IDPC = '1.3' OR v025_.IDPC like '2.%') ) THEN '1.0'
                                   ELSE '3.0' END) P_CEL_	
        FROM IES.T_MEDICAL_REQUEST_ITEM mri
        LEFT JOIN IES.T_SCHET_SLUCH_ACCOMPLISHED sa ON sa.SchetSluchAccomplishedID = mri.SchetSluchAccomplished
        --LEFT JOIN IES.T_SCHET_SLUCH ss ON sa.SchetSluchAccomplishedID=ss.SchetSluchAccomplished
        LEFT JOIN IES.T_SCHET_ZAP sz ON sz.SchetZapID=sa.SchetZap
        LEFT JOIN IES.T_SCHET s ON s.SchetID=sz.Schet
                    
        WHERE mri.IsMEK=1 AND mri.REPORT_PERIOD_MED_REG='{period_for_remek}' AND	
            s.type_=693 AND 
            --ss.IsDelete=0 AND
            sa.IsDelete=0 
    ) sa_
    GROUP BY sa_.LPU
    ) a    
    '''
    global remek_sums
    remek_sums = printmsg('[3/3]выбираем повторный МЭК', cursor, sqlstring)
    remek_sums = {x['LPU']: x for x in remek_sums}

    #"Суммируем" оба МЭКа
    all_mek_sums = zip_dicts(primary_mek_sums, remek_sums)

    #перегоняем в эксел
    wb = openpyxl.load_workbook(rep_template)
    sheet = wb[sheetname]  # может быть стоит вынести в конфигуацию имя листа... или просто брать активный лист
    mek_offset=9
    for r in openpyxl.utils.cell.rows_from_range(rangestr):
        j = openpyxl.utils.cell.coordinate_to_tuple(r[0])[0]
        startcol = openpyxl.utils.cell.coordinate_to_tuple(r[0])[1]
        lpucode=wb['Список ЛПУ'].cell(row=j, column=1).value
        if (lpucode != None) and (str(lpucode) in invoiced_sums.keys() ):
            sheet.cell(row=j, column=startcol + 1).value = invoiced_sums[str(lpucode)]['P_PREVENTION']
            sheet.cell(row=j, column=startcol).value = invoiced_sums[str(lpucode)]['P']
            sheet.cell(row=j, column=startcol + 4).value = invoiced_sums[str(lpucode)]['S']
            sheet.cell(row=j, column=startcol + 2).value = invoiced_sums[str(lpucode)]['P_URGENCY']
            sheet.cell(row=j, column=startcol + 5).value = invoiced_sums[str(lpucode)]['S_HMP']
            sheet.cell(row=j, column=startcol + 3).value = invoiced_sums[str(lpucode)]['P_ILLNESS']
            sheet.cell(row=j, column=startcol + 6).value = invoiced_sums[str(lpucode)]['Z']
            sheet.cell(row=j, column=startcol + 7).value = invoiced_sums[str(lpucode)]['A']
        if (lpucode != None) and (str(lpucode) in all_mek_sums.keys()):
            sheet.cell(row=j, column=startcol + mek_offset).value = all_mek_sums[str(lpucode)]['P']
            sheet.cell(row=j, column=startcol + mek_offset + 1).value = all_mek_sums[str(lpucode)]['P_PREVENTION']
            sheet.cell(row=j, column=startcol + mek_offset + 2).value = all_mek_sums[str(lpucode)]['P_URGENCY']
            sheet.cell(row=j, column=startcol + mek_offset + 3).value = all_mek_sums[str(lpucode)]['P_ILLNESS']
            sheet.cell(row=j, column=startcol + mek_offset + 4).value = all_mek_sums[str(lpucode)]['S']
            sheet.cell(row=j, column=startcol + mek_offset +5).value = all_mek_sums[str(lpucode)]['S_HMP']
            sheet.cell(row=j, column=startcol + mek_offset +6).value = all_mek_sums[str(lpucode)]['Z']
            sheet.cell(row=j, column=startcol + mek_offset+ 7).value = all_mek_sums[str(lpucode)]['A']


    #добиваем в эксель всякую лирику
    sqlstring=f"SELECT Top 1 t0.NAM_SMOK FROM IES.T_F002_SMO t0 WHERE (t0.SMOCOD='{smo}')"
    cursor.execute(sqlstring)
    sheet['A4'].value=f'по страховой медицинской организациии {list(cursor)[0]["NAM_SMOK"]} за {month} {year} г.         '

    wb.save(report_filename)

    pprint('Выставленные суммы:')
    pprint(invoiced_sums)

    pprint('\n \nМЭК:')
    pprint(all_mek_sums)



if __name__ == "__main__":
    main()